# DataGrabberExercice

## Prerequisites

* Need Java >8 
* Gradle 6.0.1

## Build

* execute command "gradle bootjar"
* jar is available in /build/libs/

## Launch
Execute command: java -jar data_grabber_exercice-0.1.0.jar [--server.port=8080]

## Exposed API 

Returns posts:  
/posts?nb_max=50&sorted_by=title&sort_way=asc&page=0

* with sorted_by: title/userId/id/body
* with sort_way: asc/desc



