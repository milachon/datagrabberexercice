package com.milachon.DataGrabberExercice.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.milachon.DataGrabberExercice.model.Posts;

public interface IPostsRepository extends PagingAndSortingRepository<Posts, Long> {

}
