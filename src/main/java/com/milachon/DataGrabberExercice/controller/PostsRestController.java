package com.milachon.DataGrabberExercice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.milachon.DataGrabberExercice.manager.PostsManager;
import com.milachon.DataGrabberExercice.model.Posts;

@RestController
@RequestMapping("/posts")
public class PostsRestController {

  @Autowired
  private PostsManager manager;

  @RequestMapping(method = RequestMethod.GET)
  public Page<Posts> getPosts(@RequestParam(name = "nb_max",
                                            defaultValue = "50") Integer nbMaxPosts,
                              @RequestParam(name = "sorted_by",
                                            defaultValue = "title") String sortedParam,
                              @RequestParam(name = "page", defaultValue = "0") Integer page,
                              @RequestParam(name = "sort_way", defaultValue = "") String way) {

    Sort sortBy = Sort.by(sortedParam);

    if (way.equals("asc")) {
      sortBy = sortBy.ascending();
    } else if (way.equals("desc")) {
      sortBy = sortBy.descending();
    }
    Pageable properties = PageRequest.of(page, nbMaxPosts, sortBy);

    return manager.getPostsList(properties);
  }
}
