package com.milachon.DataGrabberExercice.manager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.milachon.DataGrabberExercice.model.Posts;
import com.milachon.DataGrabberExercice.repository.IPostsRepository;

@Service
public class PostsManager {

  private static final Logger LOGGER    = LoggerFactory.getLogger(PostsManager.class);

  @Autowired
  private IPostsRepository    repo;

  public static final String  POSTS_URL = "https://jsonplaceholder.typicode.com/posts";

  public Page<Posts> getPostsList(Pageable properties) {
    return repo.findAll(properties);
  }

  /**
   * Refresh postsList data by grabbing them from POSTS_URL
   */
  @Scheduled(fixedRate = 10000)
  public void refreshPostsData() {

    try {

      /** read data at POSTS_URL as string **/
      // FIXME ObjectMapper should be able to directly call the URL, but it
      // failed
      String jsonStr = getUrlData(POSTS_URL);

      /** use json mapping **/
      List<Posts> postsList = translateJSONData(jsonStr);

      /** update repository **/
      // TODO develop update process instead of replace the list
      repo.deleteAll();
      repo.saveAll(postsList);

      LOGGER.info("Posts list from " + POSTS_URL + " has been refreshed.");
    } catch (IOException e) {
      // TODO Exception management
      LOGGER.warn("Unable to read data from " + POSTS_URL);
      e.printStackTrace();
    }
  }

  private String getUrlData(String url) throws MalformedURLException, IOException {

    String urlData = "";
    URL urlObj = new URL(url);
    URLConnection conn = urlObj.openConnection();
    try (BufferedReader reader = new BufferedReader(
        new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8))) {
      urlData = reader.lines().collect(Collectors.joining("\n"));
    }

    return urlData;
  }

  private List<Posts> translateJSONData(String data)
      throws JsonMappingException, JsonProcessingException {

    ObjectMapper mapper = new ObjectMapper();
    CollectionType type = mapper.getTypeFactory().constructCollectionType(List.class, Posts.class);

    return mapper.readValue(data, type);
  }

}
